#!/bin/bash
read -p "Enter the number of CPUs you have available: " arg1
cd kaldi/tools/ 
make -j $arg1 
cd ../src 
./configure
make -j $arg1
